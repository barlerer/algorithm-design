package resit.exam_2018_2019;

import java.io.*;
import java.util.*;

/**
 * WARNING: The spec tests are not necessarily equal to your grade!
 * You can use them help you test for the correctness of your algorithm,
 * but the final grade is determined by a manual inspection of your implementation.
 */
class AntiqueRoadshow {

    public static /**
     *  You should implement this method.
     *  @param n the number of items.
     *  @param v an array of size n+1, containing the values v_1 through v_n. You should ignore v[0].
     *  @param skip an array of size n+1, containing the values skip(1) through skip(n). You should ignore skip[0].
     *  @return The maximum value obtainable from these jobs.
     */
    int solve(int n, int[] v, int[] skip) {
        // TODO
        int[] mem = new int[n + 1];
        for (int i = 1; i <= n; i++) {
            for (int j = 0; j < (i - skip[i]); j++) {
                mem[i] = Math.max(mem[i - 1], v[i] + mem[j]);
            }
        }
        return mem[n];

    }
}