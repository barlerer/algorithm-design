package resit.exam_2018_2019;

import java.util.*;

/**
 * WARNING: The spec tests are not equal to your grade!
 * You can use them help you test for the correctness of your algorithm,
 * but the final grade is determined solely by a manual inspection of your implementation.
 */
class AuctionTime {

    /**
     *   You should implement this method.
     *   @param n the number of items.
     *   @param V the maximum value you can put up for auction.
     *   @param v an array of size n+1, containing the values v_1 through v_n. You should ignore v[0].
     *   @param M The memory array of dimension (n+1)*(V+1) filled based on the recursive formula.
     *   @return A list of all indices of the items that should be put up for auction, in _decreasing_ order.
     */
    public static List<Integer> solve(int n, int V, int[] v, int[][] M) {
        int i = n;
        List<Integer> result = new ArrayList<>();
        while (i>0) {
            if (M[i][V] == M[i - 1][V]) {
                i--;
            }
            else {
                result.add(i);
                V = V - v[i];
                i--;
            }
        }
        return result;
    }
}

