package AlgorithmDesignExercises.GreedyAlgorithms;

class PackingTrucks {

    public static /**
     * @param n the number of packages
     * @param weights the weights of all packages 1 through n. Note that weights[0] should be ignored!
     * @param maxWeight the maximum weight a truck can carry
     * @return the minimal number of trucks required to ship the packages _in the given order_.
     */
    int minAmountOfTrucks(int n, int[] weights, int maxWeight) {
        // TODO
        if (n == 0) {
            return n;
        }
        int trucks = 1;
        int currentWeight = 0;
        for (int i = 1; i <= n; i++) {
            if (currentWeight + weights[i] > maxWeight) {
                trucks++;
                currentWeight = 0;
            }
            currentWeight = currentWeight + weights[i];
        }
        return trucks;
    }
}

