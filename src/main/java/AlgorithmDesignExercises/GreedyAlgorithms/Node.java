package AlgorithmDesignExercises.GreedyAlgorithms;

import java.util.ArrayList;
import java.util.List;

class Node {

	List<Node> outgoingEdges;

	boolean marked;
	int nodeNum = -1;
	public Node() {
		this.outgoingEdges = new ArrayList<>();
		this.marked = false;
	}
}
