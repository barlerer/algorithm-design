package AlgorithmDesignExercises.GreedyAlgorithms;

public class MyUnionFind {

  private int[] parent;

  private int[] rank;

  // Union Find structure implemented with two arrays for Union by Rank
  public MyUnionFind(int size) {
    parent = new int[size];
    rank = new int[size];
    for (int i = 0; i < size; i++) parent[i] = i;
  }

  /**
   * Merge two subtrees if they have a different parent, input is array indices
   * @param i a node in the first subtree
   * @param j a node in the second subtree
   * @return true iff i and j had different parents.
   */
  boolean union(int i, int j) {
  // TODO
	  int parent1 = find(i);
	  int parent2 = find(j);
	  if (parent1==parent2) {
		  return false;
	  }
	  if (getRank()[parent1]>getRank()[parent2]) {
		  getParent()[parent2] = parent1;
	  }
	  else if (getRank()[parent1]<getRank()[parent2]) {
		  getParent()[parent1] = parent2;
	  }
	  else  {
		  getParent()[parent2] = parent1;
		  getRank()[parent1]++;
	  }
	  return true;
  }

  /**
   * NB: this function should also do path compression
   * @param i index of a node
   * @return the root of the subtree containg i.
   */
  int find(int i) {
	  if (getParent()[i]==i) {
		  return i;
	  }
	  return find(getParent()[i]);
  }

  // Return the rank of the trees
  public int[] getRank() {
    return rank;
  }

  // Return the parent of the trees
  public int[] getParent() {
    return parent;
  }
}
