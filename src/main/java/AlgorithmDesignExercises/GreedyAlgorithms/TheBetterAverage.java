package AlgorithmDesignExercises.GreedyAlgorithms;

import java.io.*;
import java.util.*;

class TheBetterAverage {

	public static double solve(int n, double[] list) {
		sort(list);
		if (n%2 == 0) {
			return (list[n/2] + list[(n/2)-1])/2.0;
		}
		return list[n/2];
		
	}

	private static void sort(double[] list) {
		mergeSort(list, 0, list.length - 1);

	}

	private static void mergeSort(double[] list, int low, int high) {
		if (low < high) {
			int middle = (low + high) / 2;
			mergeSort(list, low, middle);
			mergeSort(list, middle + 1, high);
			mergeTheLists(list, low, middle, high);
		}
	}

	private static void mergeTheLists(double[] list, int low, int middle, int high) {
		double[] left = new double[middle - low +1];
		double[] right = new double[high - middle];
		for (int i = 0; i < left.length;i++) {
			left[i] = list[low + i]; 
		}
		
		for (int i = 0; i < right.length;i++) {
			right[i] = list[middle + 1 + i];
		}
		
		int i = 0;
		int k = 0;
		int j = low;
		
		while (i < left.length && k < right.length) {
			if (left[i] < right[k]) {
				list[j] = left[i];
				i++;
			}
			else {
				list[j] = right[k];
				k++;
			}
			j++;
		}
		
		while (i < left.length) {
			list[j] = left[i];
			i++;
			j++;
		}
		
		while (k < right.length) {
			list[j] = right[k];
			k++;
			j++;
		}
	}
}
