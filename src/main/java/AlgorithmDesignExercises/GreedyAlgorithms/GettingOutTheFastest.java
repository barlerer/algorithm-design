package AlgorithmDesignExercises.GreedyAlgorithms;

import java.io.*;
import java.util.*;

class GettingOutTheFastest {

	// Implement the solve method to return the answer to the problem posed by the
	// inputstream.
	public static String run(InputStream in) {
		return new GettingOutTheFastest().solve(in);
	}

	public String solve(InputStream in) {
		Scanner sc = new Scanner(in);
		/*
		 * We already parse the input for you and should not need to make changes to
		 * this part of the code. You are free to change this input format however.
		 */
		int n = sc.nextInt();
		int m = sc.nextInt();
		int s = sc.nextInt();
		int t = sc.nextInt();
		ArrayList<HashMap<Integer, Integer>> nodes = new ArrayList<>();
		for (int i = 0; i <= n; i++) {
			nodes.add(new HashMap<>());
		}
		for (int i = 0; i < m; i++) {
			int u = sc.nextInt();
			int v = sc.nextInt();
			int cost = sc.nextInt();
			nodes.get(u).put(v, cost);
		}
		// TODO
		sc.close();
		HashMap<Integer, Integer> shortest = new HashMap<>();
		for (int i = 1; i <= n; i++) {
			shortest.put(i, Integer.MAX_VALUE);
			if (i == s) {
				shortest.put(i, 0);
			}
		}
		Queue<Integer> q = new LinkedList<>();
		q.add(s);
		while (!q.isEmpty()) {
			int current = q.remove();
			HashMap<Integer, Integer> neighboors = nodes.get(current);
			for (HashMap.Entry<Integer, Integer> entry : neighboors.entrySet()) {
				int node = entry.getKey();
				int cost = entry.getValue();
				int weight = shortest.get(current) + cost + neighboors.size();
				if (weight < shortest.get(node)) {
					shortest.put(node, weight);
					q.add(node);
				}
			}
		}
		if (shortest.get(t) == Integer.MAX_VALUE) {
			return "-1";
		}
		return Integer.toString(shortest.get(t));
	}
}