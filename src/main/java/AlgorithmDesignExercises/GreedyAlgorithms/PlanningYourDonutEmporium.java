package AlgorithmDesignExercises.GreedyAlgorithms;

import java.io.*;
import java.util.*;

class PlanningYourDonutEmporium {

  // Implement the solve method to return the answer to the problem posed by the inputstream.
  public static String run(InputStream in) {
    return new PlanningYourDonutEmporium().solve(in);
  }

  public String solve(InputStream in) {
    Scanner sc = new Scanner(in);
    int n = sc.nextInt();
    int k = sc.nextInt();
    List<House> houses = new ArrayList<>(n);
    for (int i = 0; i < n; i++) {
      houses.add(new House(i, sc.nextInt(), sc.nextInt()));
    }
    int m = n * (n - 1) / 2;
    List<Distance> distances = new ArrayList<>(m);
    for (int i = 0; i < n; i++) {
      for (int j = i + 1; j < n; j++) {
        distances.add(new Distance(houses.get(i), houses.get(j)));
      }
    }
    UnionFind unionFind = new UnionFind(houses);
    // TODO
    Collections.sort(distances,(x, y)-> Long.compare(x.distance, y.distance));
    for (Distance distance: distances) {
    	if (unionFind.clusters().size()==k) {
    		break;
    	}
    	unionFind.join(distance.a, distance.b);
    }
    String res = "";
    for (Collection<House> cluster: unionFind.clusters()) {
    	Double xAvg = (double) 0;
    	Double yAvg = (double) 0;
    	for (House house: cluster) {
    		xAvg += house.x;
    		yAvg += house.y;
    	}
    	res+= xAvg/(double)cluster.size() + " " + yAvg/(double)cluster.size() + "\n";
    }
    return res;
  }
}

class distanceComparator implements Comparator<Distance> {

	@Override
	public int compare(Distance o1, Distance o2) {
		return Long.compare(o1.distance, o2.distance);
	}
	
}

