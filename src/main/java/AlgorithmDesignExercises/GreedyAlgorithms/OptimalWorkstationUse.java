package AlgorithmDesignExercises.GreedyAlgorithms;

import java.util.*;


	class OptimalWorkstationUse {

  public static /**
   * @param n number of researchers
   * @param m number of processes
   * @param start start times of jobs 1 through n. NB: you should ignore start[0]
   * @param duration duration of jobs 1 through n. NB: you should ignore duration[0]
   * @return the number of unlocks that can be saved.
   */
  int solve(int n, int m, int[] start, int[] duration) {
  // TODO
	  ArrayList<Job> jobs = new ArrayList<>();
	  for (int i=1;i<=n;i++) {
		 jobs.add(new Job(start[i], duration[i]));
	  }
	  int result = 0;
	  Collections.sort(jobs);
	  PriorityQueue<Computer> q = new PriorityQueue<>();
	  for (Job job: jobs) {
		while (!q.isEmpty()) {
			if (q.peek().available>job.start) {
				break;
			}
			Computer computer = q.poll();
			if (computer.available + m >= job.start) {
				result++;
				break;
			}
		}
		q.add(new Computer(job.finish));
	  }
	  
	  return result;
  }
}

class Computer implements Comparable<Computer> {
	int available;

	public Computer(int available) {
		this.available = available;
	}

	@Override
	public int compareTo(Computer o) {
		return this.available - o.available;
	}
}

class Job implements Comparable<Job> {
	int start;
	int duration;
	int finish;
	
	public Job(int start, int duration) {
		this.start = start;
		this.duration = duration;
		this.finish = start + duration;
	}

	@Override
	public int compareTo(Job o) {
		return this.start-o.start;
	}
}
