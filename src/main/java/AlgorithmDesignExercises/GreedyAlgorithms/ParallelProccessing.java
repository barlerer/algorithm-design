package AlgorithmDesignExercises.GreedyAlgorithms;


import java.util.*;

class ParallelProccessing {

  public static /**
   * @param n the number of jobs
   * @param m the number of processors
   * @param deadlines the deadlines of the jobs 1 through n. NB: you should ignore deadlines[0]
   * @return the minimised maximum lateness.
   */
  int solve(int n, int m, int[] deadlines) {
	  int max = 0;
	  PriorityQueue<Integer> q = new PriorityQueue<>();
	  for (int i=1;i<=n;i++) {
		  q.add(deadlines[i]);
	  }
	  int clock = 0;
	  int counter=0;
	  while (!q.isEmpty()) {
		  int i= q.poll();
		  int temp = clock - i + 1;
		  if (temp>max) {
			  max = temp;
		  }
		  counter++;
		  if (counter==m) {
			  clock++;
			  counter=0;
		  }
	  }
	  return max;
  }
}
