package AlgorithmDesignExercises.GreedyAlgorithms;

import java.io.*;
import java.util.*;

class RoutingTrains {
	int n;
	int m;
	int start;
	Node[] nodes;
	// Implement the solve method to return the answer to the problem posed by the
	// inputstream.
	public static String run(InputStream in) {
		return new RoutingTrains().solve(in);
	}

	public String solve(InputStream in) {
		parseInput(in);
		Queue<Integer> q = new LinkedList<>();
		q.add(start);
		while (!q.isEmpty()) {
			int currentNode = q.poll();
			
			if (nodes[currentNode-1].marked == true ) {
				return "yes";
			}
			nodes[currentNode-1].marked = true;
			for (Node neighbors: nodes[currentNode-1].outgoingEdges) {
				q.add(neighbors.nodeNum);
			}
		}
		return "no";
	}

	private void parseInput(InputStream in) {
		Scanner sc = new Scanner(in);
		this.n = sc.nextInt();
		this.m = sc.nextInt();
		this.start = sc.nextInt();
		this.nodes = new Node[n];
		for (int i = 1;i <= n;i++) {
			nodes[i-1] = new Node();
			nodes[i-1].nodeNum = i;
		}
		for (int i = 0;i < m;i++) {
			int tempFrom = sc.nextInt();
			int tempTo = sc.nextInt();
			int weight = sc.nextInt();
			nodes[tempFrom-1].outgoingEdges.add(nodes[tempTo-1]);
		}
		sc.close();
	}
}

