package AlgorithmDesignExercises.GreedyAlgorithms;

import java.io.*;
import java.util.*;

public class GettingOutOfMaze {
	int n;
	int m;
	int from;
	int to;
	Node[] nodes;
	// Implement the solve method to return the answer to the problem posed by the
	// inputstream.
	public static String run(InputStream in) {
		return new GettingOutOfMaze().solve(in);
	}

	public String solve(InputStream in) {
		// TODO
		parseInput(in);
		Queue<Integer> q = new LinkedList<>();
		q.add(from);
		while (!q.isEmpty()) {
			int currentNode = q.poll();
			nodes[currentNode-1].marked = true;
			if (currentNode == to ) {
				return "yes";
			}
			for (Node neighbors: nodes[currentNode-1].outgoingEdges) {
				if (!nodes[neighbors.nodeNum-1].marked)
				q.add(neighbors.nodeNum);
			}
		}
		return "no";
	}

	private void parseInput(InputStream in) {
		Scanner sc = new Scanner(in);
		this.n = sc.nextInt();
		this.m = sc.nextInt();
		this.from = sc.nextInt();
		this.to = sc.nextInt();
		this.nodes = new Node[n];
		for (int i = 1;i <= n;i++) {
			nodes[i-1] = new Node();
			nodes[i-1].nodeNum = i;
		}
		for (int i = 0;i < m;i++) {
			int tempFrom = sc.nextInt();
			int tempTo = sc.nextInt();
			int weight = sc.nextInt();
			nodes[tempFrom-1].outgoingEdges.add(nodes[tempTo-1]);
		}
		sc.close();
	}
}
