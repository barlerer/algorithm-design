package AlgorithmDesignExercises.GreedyAlgorithms;

import java.io.*;
import java.util.*;

class PlanningBikeRepair {
	int jobs;
	ArrayList<BikeRepair> bikes;

	// Implement the solve method to return the answer to the problem posed by the
	// inputstream.
	public static String run(InputStream in) {
		return new PlanningBikeRepair().solve(in);
	}

	public String solve(InputStream in) {
		bikes = new ArrayList<>();
		parseInput(in);
		Collections.sort(bikes);
		int max = 0;
		for (int i=1;i<bikes.size();i++) {
			int collisions = 1;
			int startTimeI = bikes.get(i).startTime;
			for(int j=0;j<i;j++) {
				int endTimeJ = bikes.get(j).endTime;

				if (startTimeI<endTimeJ) {
					collisions++;
				}
				
			}
			if (collisions>max) {
				max = collisions;
			}
		}
		return Integer.toString(max);
	}

	private void parseInput(InputStream in) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(in);
		this.jobs = sc.nextInt();
		for (int i=0;i<jobs;i++) {
			int startTime = sc.nextInt();
			int duration = sc.nextInt();
			BikeRepair bikeRepair = new BikeRepair(startTime, duration);
			bikes.add(bikeRepair);
		}
		sc.close();
	}
}

class BikeRepair implements Comparable<BikeRepair> {
	int startTime;
	int endTime;
	int durationOfFix;

	BikeRepair(int startTime, int durationOfFix) {
		this.startTime = startTime;
		this.durationOfFix = durationOfFix;
		this.endTime = startTime + durationOfFix;
	}

	public int compareTo(BikeRepair other) {
		return this.startTime - other.startTime;
	}
}
