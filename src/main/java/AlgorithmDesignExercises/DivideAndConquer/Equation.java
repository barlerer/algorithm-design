package AlgorithmDesignExercises.DivideAndConquer;

abstract class Equation {

	public abstract long evaluate(long x);
}
