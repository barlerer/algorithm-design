package AlgorithmDesignExercises.DivideAndConquer;


public class ExploringAMaze {

	public boolean search(BinaryTree tree, int element) {
		if (tree == null) {
			return false;
		}
		
		if (tree.getKey() == element) {
			return true;
		}
		
		return search(tree.getLeft(),element) || search(tree.getRight(),element);
	}
}
