package AlgorithmDesignExercises.DivideAndConquer;

import java.util.*;

public class ClosestPair {

    /**
     * Takes a list of points and returns the distance between the closest pair.
     * This is done with divide and conquer.
     *
     * @param points - list of points that need to be considered.
     * @return smallest pair-wise distance between points.
     */
    public static double closestPair(List<Point> points) {
        // TODO
        Util.bruteForce(points);
        double result = findClosePair(points, 0, points.size());
        return result;
    }

    private static double findClosePair(List<Point> points, int low, int high) {
        if (high - low <= 3) {
            return Util.bruteForce(points.subList(low, high));
        }
        int middle = (low + high) / 2;
        double leftDistance = findClosePair(points, low, middle);
        double rightDistance = findClosePair(points, middle + 1, high);
        double delta = Math.min(leftDistance, rightDistance);
        List<Point> strip = new ArrayList<>();
        for (Point point: points) {
            if (Math.abs(point.x - points.get(middle).x) < delta) {
                strip.add(point);
            }
        }
        double stripDistance = findStripClosest(strip);
        return Math.min(stripDistance, delta);
    }

    private static double findStripClosest(List<Point> strip) {
        Util.sortByY(strip);
        double distance = Double.MAX_VALUE;
        for (int i=0;i<strip.size();i++) {
            for (int j=1;j<=11;j++) {
                if (j + i < strip.size()) {
                    double temp = Util.distance(strip.get(i),strip.get(j + i));
                    distance = Math.min(temp, distance);
                }
            }
        }
        return distance;
    }
}
