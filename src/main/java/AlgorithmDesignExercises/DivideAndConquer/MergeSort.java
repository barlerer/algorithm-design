package AlgorithmDesignExercises.DivideAndConquer;

public class MergeSort {

	/**
	 * Takes an array and sorts it in an ascending order.
	 *
	 * @param arr - the array that needs to be sorted.
	 */
	public void sort(int[] arr) {
		sort(arr, 0, arr.length - 1);
	}

	private void sort(int[] arr, int low, int high) {
		if (low < high) {
			int middle = (low + high) / 2;
			sort(arr, low, middle);
			sort(arr, middle + 1, high);
			merge(arr, low, middle, high);
		}
	}

	private void merge(int[] arr, int low, int middle, int high) {
		int[] left = new int[middle - low + 1];
		int[] right = new int[high - middle];
		for (int i=0;i<left.length;i++) {
			left[i] = arr[low + i];
		}
		for (int i=0;i<right.length;i++) {
			right[i] = arr[middle + 1 + i];
		}
		int mainIndex = low;
		int leftIndex = 0;
		int rightIndex = 0;

		while (leftIndex<left.length && rightIndex<right.length) {
			if (left[leftIndex] < right[rightIndex]) {
				arr[mainIndex] = left[leftIndex];
				leftIndex++;
			}
			else if (left[leftIndex] >= right[rightIndex]) {
				arr[mainIndex] = right[rightIndex];
				rightIndex++;
			}
			mainIndex++;
		}

		while (leftIndex<left.length) {
			arr[mainIndex] = left[leftIndex];
			leftIndex++;
			mainIndex++;
		}

		while (rightIndex<right.length) {
			arr[mainIndex] = right[rightIndex];
			rightIndex++;
			mainIndex++;
		}
	}
}