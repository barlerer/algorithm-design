package AlgorithmDesignExercises.DivideAndConquer;


class SortingAndCounting {
	static int count = 0;

	static int countInversions(int[] array) {
		mergeAndCount(array, 0, array.length -1);
		int temp = count;
		count = 0;
		return temp;
	}

	private static void mergeAndCount(int[] array, int low, int high) {
		if (low < high) {
			int middle = (high + low) / 2;
			mergeAndCount(array, low, middle);
			mergeAndCount(array, middle + 1, high);
			merge(array, low, middle, high);
		}
	}

	private static void merge(int[] array, int low, int middle, int high) {
		int[] left = new int[middle - low + 1];
		int[] right = new int[high-middle];
		for (int i=0;i<left.length;i++) {
			left[i] = array[low + i];
		}
		for (int j=0;j<right.length;j++) {
			right[j] = array[middle + 1 + j];
		}
		
		int leftIndex = 0;
		int rightIndex = 0;
		int mainIndex = low;
		while (leftIndex<left.length && rightIndex<right.length) {
			if (right[rightIndex] < left[leftIndex]) {
				array[mainIndex] = right[rightIndex];
				rightIndex++;
				count = count + left.length - leftIndex;
			}
			else {
				array[mainIndex] = left[leftIndex];
				leftIndex++;
			}
			mainIndex++;
		}
		
		while (leftIndex<left.length) {
			array[mainIndex] = left[leftIndex];
			leftIndex++;
			mainIndex++;
		}
		
		while (rightIndex<right.length) {
			array[mainIndex] = right[rightIndex];
			rightIndex++;
			mainIndex++;
		}
	}
}
