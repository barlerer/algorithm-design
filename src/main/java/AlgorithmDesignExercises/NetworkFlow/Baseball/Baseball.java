package AlgorithmDesignExercises.NetworkFlow.Baseball;

import java.io.*;
import java.util.*;

class Solution {

    /**
     * Returns true if team x can still win the Cup.
     */
    public static boolean solve(InputStream in) {
        Scanner sc = new Scanner(in);
        int m = sc.nextInt();
        int[] w = new int[m + 1];
        int[] g = new int[m + 1];
        HashMap<Integer, List<Integer>> games = new HashMap<>();
        for (int i = 1; i<=m;i++) {
            games.put(i, new ArrayList<>());
        }
        List<Node> nodes = new ArrayList<>();
        Node s = new Node(0);
        Node t = new Node(m + 1);
        nodes.add(s);

        // Creating nodes for teams
        for (int i = 1;i <= m; i++) {
            Node node = new Node(i);
            nodes.add(node);
        }

        // Game scanning, there will be duplicated but I will deal with them later.
        int teamX = sc.nextInt();
        w[1] = sc.nextInt();
        g[1] = sc.nextInt();
        int totalPossibleWins = w[1] + g[1];
        for (int i=1; i<=g[1];i++) {
            sc.nextInt();
        }
        for (int i = 2;i <= m; i++) {
            int teamId = sc.nextInt();
            w[i] = sc.nextInt();
            g[i] = sc.nextInt();
            for (int j = 1;j <= g[i];j++) {
                games.get(i).add(sc.nextInt());
            }
        }


        return false;

        //To add sink to nodes
    }

}

