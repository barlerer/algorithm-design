package AlgorithmDesignExercises.NetworkFlow.ImageSegmentation;

import java.io.*;
import java.util.*;

class ImageSegmentation {
    int punish = 10;
    int n;
    int m;
    int[] f;
    int[] b;
    List<Node> nodes;
    int[][] edges;

    public int solve(InputStream in) {
        parse(in);
        Node s = new Node(0);
        Node t = new Node(n + 1);
        nodes = new ArrayList<>();
        nodes.add(s);
        for (int i=1;i<=n;i++) {
            Node temp = new Node(i);
            nodes.add(temp);
            s.addEdge(temp, f[i]);
            temp.addEdge(t, b[i]);
        }
        for (int i = 1;i<=m;i++) {
            int from = edges[1][i];
            int to = edges[2][i];
            nodes.get(from).addEdge(nodes.get(to), 10);
            nodes.get(to).addEdge(nodes.get(from), 10);
        }
        //To add sink into list of node
        nodes.add(t);
        Graph g = new Graph(nodes, s, t);
        MaxFlow.maximizeFlow(g);
        int f=0;
        for (Edge edge: s.getEdges()) {
            f+=edge.getFlow();
        }
        return f;
    }

    private void parse(InputStream in) {
        Scanner sc = new Scanner(in);
        this.n = sc.nextInt();
        this.m = sc.nextInt();
        this.f = new int[n + 1];
        this.b = new int[n + 1];
        for (int i = 1; i <= n; i++) {
            sc.nextInt();
            f[i] = sc.nextInt();
            b[i] = sc.nextInt();
        }
        edges = new int[m + 2][m + 2];

        for (int i = 1; i <= m; i++) {
            edges[1][i] = sc.nextInt();
            edges[2][i] = sc.nextInt();
        }
        sc.close();
    }
}
