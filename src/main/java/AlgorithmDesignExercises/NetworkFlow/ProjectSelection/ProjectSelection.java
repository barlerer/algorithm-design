package AlgorithmDesignExercises.NetworkFlow.ProjectSelection;

import java.io.*;
import java.util.*;

class ProjectSelection {

    public boolean solve(InputStream in) {
        Scanner sc = new Scanner(in);
        //number of people
        int n = sc.nextInt();
        //number of jobs
        int m = sc.nextInt();
        Set[] skills = new Set[n + m + 1];
        int[] times = new int[n + m + 1];
        int[] specialties = new int[n + m + 1];

        //creating people nodes
        List<Node> nodes = new ArrayList<>();
        Node s = new Node(Integer.toString(0));
        Node t = new Node(Integer.toString(n + m + 1));

        nodes.add(s);
        //1 to n are people
        for (int i = 0;i<skills.length;i++) {
            skills[i] = new HashSet();
        }
        for (int i = 1; i <= n; i++) {
            nodes.add(new Node(Integer.toString(i)));
            sc.next();
            times[i] = sc.nextInt();
            specialties[i] = sc.nextInt();
            for (int j = 0; j < specialties[i]; j++) {
                skills[i].add(sc.next());
            }
            //Connecting source to all people
            s.addEdge(nodes.get(i), times[i]);
        }

        //n + 1 until n + m are jobs
        int jobsTime = 0;
        for (int i = n + 1; i <= n + m; i++) {
            nodes.add(new Node(Integer.toString(i)));
            sc.next();
            times[i] = sc.nextInt();
            jobsTime+=times[i];
            specialties[i] = sc.nextInt();
            for (int j = 0; j < specialties[i]; j++) {
                skills[i].add(sc.next());
            }
            //Connecting jobs to sink
            nodes.get(i).addEdge(t, times[i]);
        }

        //Making the connections between people and jobs.
        for (int i = 1; i <= n; i++) {
            Node p = nodes.get(i);
            for (int j = n + 1; j <= n + m; j++) {
                if (skills[i].containsAll(skills[j])) {
                    p.addEdge(nodes.get(j), Integer.MAX_VALUE / 2);
                }
            }
        }

        //Add sink to nodes
        nodes.add(t);
        Graph g = new Graph(nodes, s, t);

        g.maximizeFlow();
        int f = 0;
        for (Edge edge: s.getEdges()) {
            f+=edge.getFlow();
        }

        return f == jobsTime;
    }
}

