package AlgorithmDesignExercises.NetworkFlow.ResidualGraph;


import java.io.*;
import java.util.*;

class ResidualGraph {

    /**
     * Parses inputstream to graph.
     */
    static Graph parse(InputStream in) {
        Scanner sc = new Scanner(in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        int s = sc.nextInt();
        int t = sc.nextInt();
        List<Node> nodes = new ArrayList<>();
        for (int i=0;i < n;i++) {
            nodes.add(new Node(i));
        }
        for (int i=0;i < m;i++) {
            int from = sc.nextInt();
            int to = sc.nextInt();
            int cap = sc.nextInt();
            int flow = sc.nextInt();
            boolean backwardsEdge = flow > 0 ?  true : false;
            boolean forwardEdge = flow < cap ? true : false;
            nodes.get(from).addEdge(nodes.get(to), cap, flow, false);

            nodes.get(to).addEdge(nodes.get(from), flow, 0, backwardsEdge);

        }
        sc.close();
        return new Graph(nodes, nodes.get(s), nodes.get(t));

    }

    /**
     * Augments the flow over the given path by 1 if possible.
     *
     * @param g    Graph to operate on.
     * @param path List of nodes to represent the path.
     * @throws IllegalArgumentException if augmenting the flow in the given path is impossible.
     */
    static void augmentPath(Graph g, List<Integer> path) throws IllegalArgumentException {
        List<Node> nodes = g.getNodes();
        for (int i = 0; i < path.size() - 1;i++) {
            Node currentNode = nodes.get(path.get(i));
            Node nextNode = nodes.get(path.get(i + 1));
            Edge edge = findEdge(currentNode, nextNode);
            if (edge == null) {
                throw new IllegalArgumentException();
            }
            if (!edge.isBackwards() && edge.getFlow()>= edge.getCapacity()) {
                throw new IllegalArgumentException();
            }
            if (edge.isBackwards() && edge.getCapacity()<= 0) {
                throw new IllegalArgumentException();
            }
            Edge backwardsEdge = findEdge(nextNode, currentNode);
            if (!edge.isBackwards()) {
                edge.setFlow(edge.getFlow() + 1);
                backwardsEdge.setCapacity(backwardsEdge.getCapacity() + 1);
            }
            else {
                edge.setFlow(edge.getFlow() - 1);
                backwardsEdge.setCapacity(backwardsEdge.getCapacity() - 1);
            }
        }
    }

    static void GraphPrint(Graph g) {
        List<Node> nodes = g.getNodes();
        for (Node node: nodes) {
            for (Edge edge: node.getEdges()) {
                System.out.println("From: ");
                System.out.println(edge.getFrom().getId());
                System.out.println("To: ");
                System.out.println(edge.getTo().getId());
                System.out.println("Capacity: ");
                System.out.println(edge.getCapacity());
                System.out.println("Flow: ");
                System.out.println(edge.getFlow());
            }
        }
    }

    static void AnotherGraphPrint(Graph g) {
        List<Node> nodes = g.getNodes();
        for (Node node: nodes) {
            for (Edge edge: node.getEdges()) {
                System.out.print(edge.getFrom().getId() + " ");
                System.out.print(edge.getTo().getId() + " ");
                System.out.print(edge.getCapacity() + " ");
                System.out.print(edge.getFlow() + " ");
                System.out.println();
            }
        }
    }

    static Edge findEdge(Node a, Node b) {
        Collection<Edge> edges = a.getEdges();
        for (Edge edge: edges) {
            if (edge.getTo().equals(b)) {
                return edge;
            }
        }
        return null;
    }
}