package AlgorithmDesignExercises.NetworkFlow.FordFulkerson;

import java.util.*;

public class FordFulkerson {
    /**
     * Find the maximum flow in the given network.
     *
     * @param g Graph representing the network.
     * @return The maximum flow of the network.
     */
    static int maxFlow(Graph g) {
        List<Edge> path = findMaxFlow(g);
        while (path.size() != 0) {
            augmentPath(path);
            path = findMaxFlow(g);
        }

        Node s = g.getSource();
        int maxFlow = 0;
        for (Edge edge : s.getEdges()) {
            maxFlow+=edge.getFlow();
        }
        return maxFlow;
    }

    private static List<Edge> findMaxFlow(Graph g) {
        List<Edge> result = new ArrayList<>();
        Node s = g.getSource();
        Node t = g.getSink();
        Queue<Node> q = new LinkedList<>();
        q.add(s);
        Node current = s;
        HashMap<Node, Edge> map = new HashMap<>();
        while (!q.isEmpty() && !current.equals(t)) {
            current = q.remove();
            Collection<Edge> edges = current.getEdges();
            for (Edge edge : edges) {
                Node nextNode = edge.getTo();
                if (nextNode != s && map.get(nextNode) == null && edge.getCapacity() - edge.getFlow() > 0) {
                    q.add(nextNode);
                    map.put(nextNode, edge);
                }
            }
        }

        Edge e = map.get(t);
        while (e != null) {
            result.add(0, e);
            e = map.get(e.from);
        }
        return result;
    }

    static void augmentPath(List<Edge> path) {
        int bottleNeck = findBottleNeck(path);
        for (Edge edge : path) {
            edge.setFlow(edge.getFlow() + bottleNeck);
            edge.getBackwards().setFlow(edge.getCapacity() - edge.getFlow());
        }
    }

    private static int findBottleNeck(List<Edge> path) {
        int result = Integer.MAX_VALUE;
        for (Edge edge : path) {
            result = Math.min(result, edge.getCapacity() - edge.getFlow());
        }
        return result;
    }

}