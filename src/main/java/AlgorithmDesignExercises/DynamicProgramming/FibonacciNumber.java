package AlgorithmDesignExercises.DynamicProgramming;

class FibonacciNumber {

    /**
     * Returns the n'th Fibonacci number
     */
    public static int fibonacci(int n) {
        int[] fibonacci = new int[n + 1];
        fibonacci[0] = 0;
        fibonacci[1] = 1;
        if (n==0 || n == 1) {
            return fibonacci[n];
        }
        for (int i = 2; i <= n; i++) {
            fibonacci[i] = fibonacci[i-1] + fibonacci[i-2];
        }
        return fibonacci[n];
    }
}


