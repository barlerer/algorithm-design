package AlgorithmDesignExercises.DynamicProgramming;

class AligningYourSequences {

    public static int solve(String firstString, String secondString) {
        char[] first = firstString.toCharArray();
        char[] second = secondString.toCharArray();
        int[][] result = new int[first.length + 1][second.length + 1];
        for (int i=0;i<=first.length;i++) {
            result[i][0] = i;
        }

        for (int j=0;j<=second.length;j++) {
            result[0][j] = j;
        }

        for (int i=1;i<=first.length;i++) {
            for (int j=1;j<=second.length;j++) {
                int temp = 1;
                if (first[i-1] == second[j - 1]) {
                    temp = 0;
                }
                result[i][j] = Math.min(Math.min(result[i - 1][j] + 1,result[i][j - 1] + 1),temp + result[i - 1][j - 1]);
            }
        }
        return result[first.length][second.length];
    }
}