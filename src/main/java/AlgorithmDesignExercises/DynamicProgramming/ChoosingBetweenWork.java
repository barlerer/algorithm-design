package AlgorithmDesignExercises.DynamicProgramming;

class ChoosingBetweenWork {

    /**
     * Come up with an iterative dynamic programming solution to the weighted interval scheduling problem.
     * NB: You may assume the jobs are sorted by ascending finishing time.
     * @param n the number of jobs
     * @param s the start times of the jobs for jobs 1 through n. Note that s[0] should be ignored.
     * @param f the finish times of the jobs for jobs 1 through n. Note that f[0] should be ignored.
     * @param v the values of the jobs for jobs 1 through n. Note that v[0] should be ignored.
     * @param p the predecessors of the jobs for jobs 1 through n. Note that p[0] should be ignored and that -1 represents there being no predecessor.
     * @return the weight of the maximum weight schedule.
     */
    public static int solve(int n, int[] s, int[] f, int[] v, int[] p) {
        int[] mem = new int[n + 1];
        mComputeOut(n, mem, v, p);
        // Because no predecessor is -1, I increment by 1.
        return mem[n] + 1;
    }

    public static int mComputeOut(int j, int[] memory, int[] weight, int[] p) {
        if (j < 1) {
            return -1;
        }
        else if (memory[j] != 0) {
            return memory[j];
        }
        memory[j] = Math.max(weight[j] + mComputeOut(p[j], memory, weight, p), mComputeOut(j - 1, memory, weight, p));
        return memory[j];
    }

}