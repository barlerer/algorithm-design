package AlgorithmDesignExercises.DynamicProgramming;

import java.util.*;

class WhatsThePreviousOne {

    public static int[] solve(int n, int[] s, int[] f, int[] v) {
        int[] result = new int[n + 1];
        ArrayList<Job> jobs = new ArrayList<>();
        for (int i=1; i<=n;i++) {
            jobs.add(new Job(s[i], f[i], v[i]));
        }
        Collections.sort(jobs);
        for (int i = 1;i< result.length;i++) {
            result[i] = findPredecessor(jobs, i - 1);
        }

        return result;
    }

    private static int findPredecessor(ArrayList<Job> jobs, int index) {
        Job job = jobs.get(index);
        int result = -1;
        for (int i = index;i>0;i--) {
            if (jobs.get(i - 1).finish<=job.start) {
                return result = i ;
            }
        }
        return result;
    }
}

class Job implements Comparable<Job> {
    int start;
    int finish;
    int weight;

    public Job(int start, int finish, int weight) {
        this.start = start;
        this.finish = finish;
        this.weight = weight;
    }

    @Override
    public int compareTo(Job o) {
        return this.finish - o.finish;
    }
}
