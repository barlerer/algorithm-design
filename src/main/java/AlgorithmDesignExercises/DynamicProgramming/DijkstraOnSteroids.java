package AlgorithmDesignExercises.DynamicProgramming;

class BellmanFord {

    public static int solve(int n, int m, int[][] graph) {
        int[][] mem = new int[n][m];
        for (int i=0;i<n;i++) {
            for (int j=0;j<m;j++) {
                mem[i][j] = Integer.MAX_VALUE;
            }
        }
        mem[0][0] = 0;
        int[] predecessor = new int[(n*m) -1];

        for (int k = 0;k<predecessor.length;k++) {
            for (int i=0;i<n;i++) {
                for (int j=0;j<m;j++) {
                    if (i == 0 && j == 0)
                        continue;
                    if (isValidMove(n, m, i, j + 1)) {
                         mem[i][j] = Integer.min(mem[i][j], Integer.max(mem[i][j + 1], graph[i][j] - graph[i][j + 1]));
                    }

                    if (isValidMove(n, m, i, j - 1)) {
                        mem[i][j] = Integer.min(mem[i][j], Integer.max(mem[i][j - 1], graph[i][j] - graph[i][j - 1]));
                    }

                    if (isValidMove(n, m, i + 1, j)) {
                        mem[i][j] = Integer.min(mem[i][j], Integer.max(mem[i + 1][j], graph[i][j] - graph[i + 1][j]));
                    }

                    if (isValidMove(n, m, i - 1, j)) {
                        mem[i][j] = Integer.min(mem[i][j], Integer.max(mem[i - 1][j], graph[i][j] - graph[i - 1][j]));
                    }
                }
            }
        }
        return mem[n-1][m-1];
    }

    private static boolean isValidMove(int n, int m, int i, int i1) {
        return (i < n && i1 < m && i>= 0 && i1 >= 0);
    }

    private static void print2DArray(int[][] mem) {
        for (int[] x : mem)
        {
            for (int y : x)
            {
                System.out.print(y + " ");
            }
            System.out.println();
        }
    }
}


