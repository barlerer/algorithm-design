package AlgorithmDesignExercises.DynamicProgramming;

class Fisherman {

    /**
     * @param n the number of days
     * @param P the profits that can be made on day 1 through n on location P are stored in P[1] through P[n].
     * @param Q the profits that can be made on day 1 through n on location Q are stored in Q[1] through Q[n].
     * @return the maximum obtainable profit.
     */
    public static int solve(int n, int[] P, int[] Q) {
        int[][] mem = new int[n+1][2];
        mem[0][0] = 0;
        mem[1][0] = P[1];
        mem[0][1] = 0;
        mem[1][1] = Q[1];
        for (int i = 2; i <= n; i++) {
            mem[i][0] = Integer.max(P[i] + mem[i-1][0], mem[i-1][1]);
            mem[i][1] = Integer.max(Q[i] + mem[i-1][1], mem[i-1][0]);
        }
        return Integer.max(mem[n][0], mem[n][1]);
    }
}

