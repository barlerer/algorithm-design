package AlgorithmDesignExercises.DynamicProgramming;

import java.io.InputStream;
import java.util.Scanner;

class AStudentBudget {
    int n;
    int W;
    int[] w;
    int[] v;

    public static String run(InputStream in) {
        return new AStudentBudget().solve(in);
    }

    public String solve(InputStream in) {
        parseInput(in);

        int[][] result = new int[n + 1][W + 1];

        for (int i = 0; i<=n; i++) {
            for (int j = 0; j<=W;j++) {
                if (i == 0 || j == 0) {
                    result[i][j] = 0;
                }
                else if (w[i-1] <= j) {
                    result[i][j] = Math.max(v[i-1] + result[i - 1][j - w[i - 1]], result[i - 1][j]);
                }
                else {
                    result[i][j] = result[i - 1][j];
                }
            }
        }
        return String.valueOf(result[n][W]);

    }

    private void parseInput(InputStream in) {
        Scanner sc = new Scanner(in);
        W = sc.nextInt();
        n = sc.nextInt();
        w = new int[n];
        v = new int[n];
        int i = 0;
        while (sc.hasNext()) {
            w[i] = sc.nextInt();
            v[i] = sc.nextInt();
            i++;
        }
        sc.close();
    }
}