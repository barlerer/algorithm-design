package Exam_2018_2019;

import java.util.*;

/**
 * WARNING: The spec tests are not equal to your grade!
 * You can use them help you test for the correctness of your algorithm,
 * but the final grade is determined solely by a manual inspection of your implementation.
 */
class FindSolution {

    /**
     *   You should implement this method.
     *   @param n The number of updates
     *   @param C the (monthly) cost budget
     *   @param benefits An array of dimension n+1 containing the benefits of all the code changes for 1 <= i <= n
     *   @param costs An array of dimension n+1 containing the costs of all the code changes for 1 <= i <= n
     *   @param M The memory array of dimension (n+1)*(C+1) filled based on the recursive formula.
     *   @return A list of all indices of the updates that should be included, in _increasing_ order.
     */
    public static List<Integer> solve(int n, int C, int[] benefits, int[] costs, int[][] M) {
        // TODO
        List<Integer> result = new ArrayList<>();
        int c=C;
        int i=n;
        while(i>0) {
            if (M[i][c] == M[i - 1][c]) {
                i--;
            }
            else {
                result.add(0, i);
                c=c-costs[i];
                i--;
            }
        }
        return result;
    }
}

