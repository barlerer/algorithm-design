package Exam_2018_2019;

/**
 * WARNING: The spec tests are not equal to your grade!
 * You can use them help you test for the correctness of your algorithm,
 * but the final grade is determined solely by a manual inspection of your implementation.
 */
class DynamicProgramming {

    /**
     *   You should implement this method. Note that your solution should be _iterative_!
     *   @param n The number of updates to ship.
     *   @param c The cost of shipping one bundle of updates.
     *   @param costs The costs matrix of dimension (n+1)*(n+1), where costs[i][j] denotes the costs of bundling updates i,i+1,...,j; given for all 1 <= i <= j <= n
     *   @return The minimal costs of bundling.
     */
    public static int solve(int n, int c, int[][] costs) {
        // TODO
        int[] mem = new int[n + 1];
        for (int i=1;i<=n;i++) {
            mem[i] = c + costs[1][i];
            for (int j=2;j<i;j++) {
                if (mem[i] > c + costs[j][i] + mem[j - 1]) {
                    mem[i] = c + costs[j][i] + mem[j - 1];
                }
            }
        }
        return mem[n];
    }
}


