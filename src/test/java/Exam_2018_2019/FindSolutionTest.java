package Exam_2018_2019;

import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FindSolutionTest {
    @Test
    public void example() {
        int n = 3;
        int C = 5;
        int[] b = { 0, 8, 7, 2 };
        int[] c = { 0, 3, 3, 2 };
        int[][] M = { { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 8, 8, 8 }, { 0, 0, 0, 8, 8, 8 }, { 0, 0, 2, 8, 8, 10 } };
        List<Integer> chosenElements = new LinkedList<>();
        chosenElements.add(1);
        chosenElements.add(3);
        assertEquals(chosenElements, FindSolution.solve(n, C, b, c, M));
    }
}
