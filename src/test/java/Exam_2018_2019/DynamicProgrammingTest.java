package Exam_2018_2019;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DynamicProgrammingTest {

    @Test
    public void example() {
        int m = 4;
        int c = 8;
        int[][] R = { { 0, 0, 0, 0, 0 }, { 0, 0, 2, 3, 4 }, { 0, 0, 0, 2, 3 }, { 0, 0, 0, 0, 2 }, { 0, 0, 0, 0, 0 } };
        assertEquals(12, DynamicProgramming.solve(m, c, R));
    }

    @Test
    public void example02() {
        int m = 4;
        int c = 8;
        int[][] R = { { 0, 0, 0, 0, 0 }, { 0, 0, 2, 100, 100 }, { 0, 0, 0, 2, 100 }, { 0, 0, 0, 0, 2 }, { 0, 0, 0, 0, 0 } };
        assertEquals(20, DynamicProgramming.solve(m, c, R));
    }
}
