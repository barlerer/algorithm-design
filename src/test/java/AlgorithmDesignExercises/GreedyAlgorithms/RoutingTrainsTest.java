package AlgorithmDesignExercises.GreedyAlgorithms;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;

import org.junit.jupiter.api.Test;

public class RoutingTrainsTest {
	
	@Test
	public void testExample() {
		String example = "5 6 1\r\n" + 
				"1 2 2\r\n" + 
				"3 2 100\r\n" + 
				"1 3 10\r\n" + 
				"4 5 10\r\n" + 
				"2 4 10\r\n" + 
				"5 3 10";

		assertEquals("yes", RoutingTrains.run(new ByteArrayInputStream(example.getBytes())));
	}


}
