package AlgorithmDesignExercises.GreedyAlgorithms;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

public class TheBetterAverageTest {
	

	  @Test
	  public void example() {
	    int n = 4;
	    double[] list = { 4, 2, 1, 3 };
	    assertEquals(2.5, TheBetterAverage.solve(n, list), 1e-3);
	  }
	  
	  @Test
	  public void exampleSorting() {
	    double[] list = { 4, 2, 1, 3 };
	    TheBetterAverage.solve(4, list);
	    System.out.println(Arrays.toString(list));
	  }
	

}
