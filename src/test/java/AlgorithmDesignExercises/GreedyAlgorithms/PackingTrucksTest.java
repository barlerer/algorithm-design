package AlgorithmDesignExercises.GreedyAlgorithms;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class PackingTrucksTest {
	
	@Test
	  public void example() {
	    int n = 4;
	    int[] weights = { 0, 41, 29, 12, 26 };
	    int maxWeight = 48;
	    assertEquals(3, PackingTrucks.minAmountOfTrucks(n, weights, maxWeight));
	  }

}
