package AlgorithmDesignExercises.GreedyAlgorithms;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Test;

public class BikeRepairTest {
	
	  @Test
	  public void example() {
		  String example = "6\r\n" + 
		  		"6 1\r\n" + 
		  		"3 5\r\n" + 
		  		"1 2\r\n" + 
		  		"6 3\r\n" + 
		  		"8 3\r\n" + 
		  		"1 4";
		    assertEquals("3", PlanningBikeRepair.run(new ByteArrayInputStream(example.getBytes(StandardCharsets.UTF_8))).trim());
	  }

}
