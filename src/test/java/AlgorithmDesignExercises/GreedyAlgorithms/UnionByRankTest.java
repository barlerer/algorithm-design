package AlgorithmDesignExercises.GreedyAlgorithms;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class UnionByRankTest {
    @Test
    public void findWithUnionTest() {
        MyUnionFind uf = new MyUnionFind(10);
        assertTrue(uf.union(4, 2));
        assertFalse(uf.union(2, 4));
        assertEquals(uf.find(2), uf.find(4));
    }
}
