package AlgorithmDesignExercises.GreedyAlgorithms;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;

import org.junit.jupiter.api.Test;

public class GettingOutOfMazeTest {

	@Test
	public void testExample() {
		String example = "7 7 1 5\r\n" + "1 2 2\r\n" + "2 3 100\r\n" + "3 4 10\r\n" + "4 5 10\r\n" + "2 6 10\r\n"
				+ "6 7 10\r\n" + "7 4 80";

		assertEquals("yes", GettingOutOfMaze.run(new ByteArrayInputStream(example.getBytes())));
	}

}
