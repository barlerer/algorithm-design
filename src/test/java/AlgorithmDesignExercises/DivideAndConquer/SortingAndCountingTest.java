package AlgorithmDesignExercises.DivideAndConquer;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SortingAndCountingTest {

    @Test
    public void countInversions() {
        int[] input = { 2, 1, 0, 8 };
        int[] sorted = { 2, 1, 0, 8 };
        Arrays.sort(sorted);
        assertEquals(3, SortingAndCounting.countInversions(input));
        assertArrayEquals(sorted, input);
    }

    @Test
    public void countInversionsNormal() {
        int[] input = { 0, 1, 2, 3 };
        int[] sorted = { 0, 1, 2, 3 };
        Arrays.sort(sorted);
        assertEquals(0, SortingAndCounting.countInversions(input));
        assertArrayEquals(sorted, input);
    }

    @Test
    public void countInversionsOdd() {
        int[] input = { 0, 1, 2, 7 ,5};
        int[] sorted = { 0, 1, 2, 7 ,5};
        Arrays.sort(sorted);
        assertEquals(1, SortingAndCounting.countInversions(input));
        assertArrayEquals(sorted, input);
    }
}
