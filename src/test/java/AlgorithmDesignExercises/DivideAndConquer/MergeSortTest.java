package AlgorithmDesignExercises.DivideAndConquer;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

public class MergeSortTest {
	@Test()
	public void testEven() {
		int[] arr = new int[] { 1, 4, 2, 0, -6, 19, 21 - 10 };
		int[] arr2 = new int[] { 1, 4, 2, 0, -6, 19, 21 - 10 };
		Arrays.sort(arr2);
		MergeSort mergeSort = new MergeSort();
		mergeSort.sort(arr);
		assertArrayEquals(arr, arr2);
	}
	
	@Test()
	public void testOdd() {
		int[] arr = new int[] { 1, 4, 2, 0, -6, 19, 21 - 10, -57, 111019, 313113131, 3, 3 };
		int[] arr2 = new int[] { 1, 4, 2, 0, -6, 19, 21 - 10, -57, 111019, 313113131, 3, 3 };
		Arrays.sort(arr2);
		MergeSort mergeSort = new MergeSort();
		mergeSort.sort(arr);
		assertArrayEquals(arr, arr2);
	}
	
	@Test()
	public void test1() {
		int[] arr = new int[] { 1};
		int[] arr2 = new int[] { 1};
		Arrays.sort(arr2);
		MergeSort mergeSort = new MergeSort();
		mergeSort.sort(arr);
		assertArrayEquals(arr, arr2);
	}

}
