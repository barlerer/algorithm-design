package AlgorithmDesignExercises.DivideAndConquer;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FindingAMinimaTest {
	@Test
	  public void testExample() {
	    Equation eq1 = new QuadraticEquation(0, 8, -240);
	    Equation eq2 = new QuadraticEquation(0, 6, -156);
	    assertEquals(42, Solution.findMin(eq1, eq2, 0, 100));
	  }
}
