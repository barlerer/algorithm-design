package AlgorithmDesignExercises.DivideAndConquer;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SearchTreeTest {

	 @Test()
	  public void example() {
	    ExploringAMaze s = new ExploringAMaze();
	    BinaryTree tree = new BinaryTree(42, new BinaryTree(1337), new BinaryTree(39));
	    assertTrue(s.search(tree, 42));
	    assertFalse(s.search(tree, 100));
	  }

}
