package AlgorithmDesignExercises.DivideAndConquer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ClosestPairTest {
	@Test
	  public void testTwoPoints() {
	    List<Point> points = new ArrayList<>();
	    points.add(new Point(1, 2));
	    points.add(new Point(4, 6));
	    Assertions.assertEquals(5, ClosestPair.closestPair(points), 5e-6);
	  }

	  @Test
	  public void testSmall() {
	    List<Point> points = new ArrayList<>();
	    points.add(new Point(2, 3));
	    points.add(new Point(12, 30));
	    points.add(new Point(40, 50));
	    points.add(new Point(5, 1));
	    points.add(new Point(12, 10));
	    points.add(new Point(3, 4));
	    assertEquals(1.4142135623730951, ClosestPair.closestPair(points), 1e-6);
	  }

}
