package AlgorithmDesignExercises.NetworkFlow.ImageSegmentation;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ImageSegmentationTest {
    @Test
    void example() {
        String example = "2 1\n" +
                "1 9 1\n" +
                "2 8 2\n" +
                "1 2";
        ImageSegmentation imageSegmentation = new ImageSegmentation();
        assertEquals(3, imageSegmentation.solve(new ByteArrayInputStream(example.getBytes(StandardCharsets.UTF_8))));
    }
}
