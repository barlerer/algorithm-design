package AlgorithmDesignExercises.NetworkFlow.ProjectSelection;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ProjectSelectionTest {

    @Test
    public void example01() {
        String example = "2 3\n" +
                "Oliver 5 2 investigation interviewing\n" +
                "Caleb 8 2 interviewing lit\n" +
                "hire_member 1 1 interviewing\n" +
                "interview_author 3 2 interviewing lit\n" +
                "solve_crime 4 1 investigation";
        ProjectSelection projectSelection = new ProjectSelection();
        boolean result = projectSelection.solve(new ByteArrayInputStream(example.getBytes(StandardCharsets.UTF_8)));
        assertTrue(result);
    }
}
