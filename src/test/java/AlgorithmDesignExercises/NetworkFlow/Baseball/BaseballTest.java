package AlgorithmDesignExercises.NetworkFlow.Baseball;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class BaseballTest {
    @Test
    void example() {
        String example = "4 \n" +
                "4 10 2\n" +
                "2 3\n" +
                "1 12 2\n" +
                "2 3 \n" +
                "2 11 3\n" +
                "1 3 4\n" +
                "3 11 3\n" +
                "1 2 4";
        Solution solution = new Solution();
        assertFalse(solution.solve(new ByteArrayInputStream(example.getBytes(StandardCharsets.UTF_8))));
    }
}
