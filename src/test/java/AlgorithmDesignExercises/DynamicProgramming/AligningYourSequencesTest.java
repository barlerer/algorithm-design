package AlgorithmDesignExercises.DynamicProgramming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AligningYourSequencesTest {
    @Test
    public void example() {
        String a = "kitten";
        String b = "sitting";
        assertEquals(3, AligningYourSequences.solve(a, b));
    }
}
