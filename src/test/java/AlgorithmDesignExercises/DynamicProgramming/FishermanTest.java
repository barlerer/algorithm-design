package AlgorithmDesignExercises.DynamicProgramming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FishermanTest {
    @Test
    public void example() {
        int n = 5;
        int[] P = { 0, 80, 30, 30, 70, 80 };
        int[] Q = { 0, 90, 60, 60, 50, 20 };
        assertEquals(300, Fisherman.solve(n, P, Q));
    }
}
