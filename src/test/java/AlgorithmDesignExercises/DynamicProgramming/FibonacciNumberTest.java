package AlgorithmDesignExercises.DynamicProgramming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FibonacciNumberTest {
    @Test
    public void example() {
        assertEquals(8, FibonacciNumber.fibonacci(6));
    }

    @Test
    public void test05() {
        assertEquals(5, FibonacciNumber.fibonacci(5));
    }
}
