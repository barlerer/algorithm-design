package AlgorithmDesignExercises.DynamicProgramming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DijkstraOnSteroidsTest {
    @Test
    public void example() {
        int n = 2;
        int m = 3;
        int[][] graph = { { 3, 5, 6 }, { 4, 2, 1 } };
        assertEquals(1, BellmanFord.solve(n, m, graph));
    }
}
