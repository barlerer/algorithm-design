package AlgorithmDesignExercises.DynamicProgramming;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UndirectedGraphTest {
    @Test
    public void example() {
        int n = 5;
        int[] nodes = { 0, 2, 1, 6, 8, 9 };
        assertEquals(17, UndirectedGraph.weight(n, nodes));
    }
}
