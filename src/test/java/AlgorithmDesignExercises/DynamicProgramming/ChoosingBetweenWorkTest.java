package AlgorithmDesignExercises.DynamicProgramming;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ChoosingBetweenWorkTest {
    @Test
    public void example() {
        int[] s = { 0, 0, 1, 3 };
        int[] f = { 0, 3, 4, 8 };
        int[] v = { 0, 3, 5, 7 };
        int[] p = { 0, -1, -1, 1 };
        assertEquals(10, ChoosingBetweenWork.solve(3, s, f, v, p));
    }
}
