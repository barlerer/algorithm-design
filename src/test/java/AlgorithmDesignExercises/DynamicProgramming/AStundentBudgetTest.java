package AlgorithmDesignExercises.DynamicProgramming;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AStundentBudgetTest {

    @Test
    public void example() {
        String example = "10 3\n" +
                "8 25\n" +
                "3 4\n" +
                "5 9";

        InputStream targetStream = new ByteArrayInputStream(example.getBytes());
        assertEquals("25", AStudentBudget.run(targetStream));
    }
}
